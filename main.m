//
//  main.m
//  Tiles
//
//  Created by Arash Z. Jahangiri on 02/17/12.
//  Copyright (c) 2012 Arash Z. Jahangiri. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
