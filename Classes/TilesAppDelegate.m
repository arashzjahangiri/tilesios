//
//  TilesAppDelegate.m
//  Tiles
//
//  Created by Arash Z. Jahangiri on 02/17/12.
//  Copyright (c) 2012 Arash Z. Jahangiri. All rights reserved.
//

#import "TilesAppDelegate.h"
#import "TilesViewController.h"

@implementation TilesAppDelegate

@synthesize window;
@synthesize viewController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
	
	return YES;
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
