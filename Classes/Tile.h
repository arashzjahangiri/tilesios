//
//  Tile.h
//  Tiles
//
//  Created by Arash Z. Jahangiri on 02/18/12.
//  Copyright (c) 2012 Arash Z. Jahangiri. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <QuartzCore/CAGradientLayer.h>
#import "CALayer.h"


@interface Tile : CAGradientLayer {
    int tileIndex;    
}

@property (nonatomic) int tileIndex;

- (void)draw;

- (void)appearDraggable;

- (void)appearNormal;

- (void)startWiggling;

- (void)stopWiggling;

@end
