//
//  CALayer.m
//  Tiles
//
//  Created by Arash Z. Jahangiri on 02/21/12.
//  Copyright (c) 2012 Arash Z. Jahangiri. All rights reserved.
//


#import "CALayer.h"


@implementation CALayer (Additions)


- (void)moveToFront {
    CALayer *superlayer = self.superlayer;
    [self removeFromSuperlayer];
    [superlayer addSublayer:self];
}


@end
