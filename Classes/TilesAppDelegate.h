//
//  TilesAppDelegate.h
//  Tiles
//
//  Created by Arash Z. Jahangiri on 02/17/12.
//  Copyright (c) 2012 Arash Z. Jahangiri. All rights reserved.
//


#import <UIKit/UIKit.h>

@class TilesViewController;

@interface TilesAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    TilesViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet TilesViewController *viewController;

@end

