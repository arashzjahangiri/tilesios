//
//  NSString.h
//  Tiles
//
//  Created by Arash Z. Jahangiri on 02/21/12.
//  Copyright (c) 2012 Arash Z. Jahangiri. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface NSString (Additions)

- (void)drawCenteredInRect:(CGRect)rect withFont:(UIFont *)font;

@end
