//
//  NSString.m
//  Tiles
//
//  Created by Arash Z. Jahangiri on 02/21/12.
//  Copyright (c) 2012 Arash Z. Jahangiri. All rights reserved.
//


#import "NSString.h"


@implementation NSString (Additions)


- (void)drawCenteredInRect:(CGRect)rect withFont:(UIFont *)font {
    CGSize size = [self sizeWithFont:font];
    
    CGRect textBounds = CGRectMake(rect.origin.x + (rect.size.width - size.width) / 2,
                                   rect.origin.y + (rect.size.height - size.height) / 2,
                                   size.width, size.height);
    [self drawInRect:textBounds withFont:font];    
}


@end
