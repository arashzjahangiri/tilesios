//
//  CALayer.h
//  Tiles
//
//  Created by Arash Z. Jahangiri on 02/21/12.
//  Copyright (c) 2012 Arash Z. Jahangiri. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <QuartzCore/CALayer.h>

@interface CALayer (Additions)

- (void)moveToFront;

@end
